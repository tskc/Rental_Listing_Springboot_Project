<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Landlord home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css">
body {
	image-rendering: pixelated;
	background:
		url("https://images.pexels.com/photos/186077/pexels-photo-186077.jpeg");
	background-size: cover;
}
</style>
</head>
<body>
	<div class="page">
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewProfileLandlord/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>
				</div>
			</div>
		</nav>
		<h1 style="float: right; margin-right: 20px;">Welcome
			<b>${pageContext.request.userPrincipal.name}</b></h1>
		<br> <br> <br> <br> <br> <br>
		<br>
		<div class="container">
			<ul>
				<li>
					<form
						action="${contextPath}/view/${pageContext.request.userPrincipal.name}/name"
						method="get">
						<button type="submit" class="btn btn-dark" id="properties">My
							Properties</button>
					</form>

					<form action="${contextPath}/add" method="get">
						<button type="submit" class="btn btn-dark" id="add-property">Add
							Property</button>
					</form>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>