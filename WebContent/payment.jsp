<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<jsp:useBean id="now" class="java.util.Date" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Payment</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
</head>
<body>

	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewProfileLandlord/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h2 style="float: right; margin-right: 30px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h2>
		<br> <br> <br> <br> <br> <br>
		<div align="center">
			<form
				action="${contextPath}/addpayment/${pageContext.request.userPrincipal.name}"
				method="get">
				<table>
					<tr>
						<td>Date</td>
						<td>: <input type="text"
							value="<fmt:formatDate value="${now}" type="both" />" name="date" /></td>
					</tr>
					<tr>
						<td>Enter Name on the Card</td>
						<td>: <input type="text" name="name"
							placeholder="Enter name on the card" required="required">
						</td>
					</tr>

					<tr>
						<td>Enter Amount</td>
						<td>: <input type="text" name="amount"
							placeholder="Enter Payment" required="required">
						</td>
					</tr>
					<tr>
						<td>Type Of Payment</td>
						<td>: <select name="type" required="required">
								<option>Select Payment Mode</option>
								<option>Debit Card</option>
								<option>Credit Card</option>
						</select></td>
					</tr>
					<tr>
						<td>Card Number</td>
						<td>: <input type="text" id="cc_no" name="cc_no"
							placeholder="Enter Card Number" size="16" maxlength="16"
							minlength="16" required="required" /> <span id="err_cc_no"
							class="jserror"> </span></td>
					</tr>
					<tr>
						<td>Expiry Date</td>
						<td>
							<table cellpadding="0" cellspacing="0">
								<tr>

									<td>: <select id="cc_exp_mm" name="cc_exp_mm"
										required="required">
											<option value="">Month</option>
											<option value="1" selected="selected">January</option>
											<option value="2">February</option>
											<option value="3">March</option>
											<option value="4">April</option>
											<option value="5">May</option>
											<option value="6">June</option>
											<option value="7">July</option>
											<option value="8">August</option>
											<option value="9">September</option>
											<option value="10">October</option>
											<option value="11">November</option>
											<option value="12">December</option>
									</select></td>
									<td width="20">&nbsp;</td>
									<td><select id="cc_exp_yyyy" name="cc_exp_yyyy">
											<option value="">Month</option>
											<option value="2020" selected="selected">2020</option>
											<option value="2021">2021</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
											<option value="2024">2024</option>
											<option value="2025">2025</option>
											<option value="2026">2026</option>
											<option value="2027">2027</option>
											<option value="2028">2028</option>
											<option value="2029">2029</option>
											<option value="2030">2030</option>
									</select></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>Security Code</td>
						<td>: <input id="cvv" type="text" size="3" name="cvv"
							maxlength="3" minlength="3" value="" required="required" />
						</td>
					</tr>
				</table>
				<br> <br> <br>
				<div align="center">
					<button type="submit" value="Confirm Payment"
						class="btn btn-primary">Confirm Payment</button>
				</div>

			</form>
			<br>
		</div>

	</div>









































	<%-- <div class="container">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>

			<h2>
				Welcome ${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()">Logout</a>
			</h2>
		</c:if>
	</div>
	<div align="right">
		<form action="${contextPath}/welcome" method="get">
			<a href="${contextPath}/welcome"> Home</a>
			<button type="submit">Home</button>
		</form>
	</div> --%>

</body>
</html>