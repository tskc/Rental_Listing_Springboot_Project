<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Agreement</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
<script type="text/javascript">
	function accept() {
		alert("Your request has been added successfully");
	}
	function decline() {
		alert("Your request has not been added, please click on accept to add your request");
	}
</script>

</head>
<body style="background: rgba(108, 146, 208, 0.5);">
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewTenantProfile/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h3 style="float: right; margin-right: 300px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h3>

		<br> <br> <br>
		<div class="container">
			<h1>
				<b>Agreement</b>
			</h1>
			<div
				style="text-decoration-color: rgba(0, 0, 0, 0); text-transform: uppercase;">
				<h2>Rent Amount</h2>
				<p>
					The rent that has to be paid by the tenant to the landlord
					throughout the term of this agreement is <b>Rs.${property.rentPerMonth}
						per month.</b> This amount shall be paid in the first week of every
					month. Failed to do so will increase rent 2% per week.
				</p>
				<h2>Security Deposit</h2>
				<p>Advance payment for six months should be paid. This security
					deposit may be used for the emergency purposes.</p>
				<br>
				<p>
					Location of premises is in <b>${property.propertyAddress},
						${property.propertyCity}</b>
				</p>
			</div>
			<br> <br> <br>
			<div>
				<ul class="agreement-buttons">
					<li>
						<form
							action="${contextPath}/accept/${property.propertyId}/${pageContext.request.userPrincipal.name}"
							method="get">
							<button type="submit" onclick="accept()" class="btn btn-primary"
								onkeypress="accept()" style="letter-spacing: 0.03em">Accept</button>
						</form>
					</li>
					<li>
						<form
							action="${contextPath}/decline/${property.propertyId}/${pageContext.request.userPrincipal.name}">
							<button type="submit" onclick="decline()" class="btn btn-primary"
								onkeypress="decline()">Decline</button>
						</form>
					</li>
					<li>
						<form
							action="${contextPath}/viewProfileLandlord/${property.landlordName}"
							method="get">
							<button type="submit" class="btn btn-primary">Landlord
								Profile</button>
						</form>
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>