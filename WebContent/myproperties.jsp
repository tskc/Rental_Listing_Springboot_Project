
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${pageContext.request.userPrincipal.name}'s Properties</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${contextPath}/resources/css/styling.css">
<style type="text/css"></style>
</head>
<body>
	<div>
		<nav class="navbar" style="background: rgb(105, 145, 255)">
			<div class="container-fluid">
				<div>
					<img src="${contextPath}/picture white.png" class="logo" />
					<ul class="navbar-nav">
						<li>
							<form action="${contextPath}/welcome" method="get">
								<button type="submit" class="btn btn-primary navbar-btn">Home</button>
							</form>
						</li>
						<li>
							<form
								action="${contextPath}/viewProfileLandlord/${pageContext.request.userPrincipal.name}"
								method="get">
								<button type="submit" class="btn btn-primary navbar-btn">My
									Profile</button>
							</form>
						</li>
						<li><c:if
								test="${pageContext.request.userPrincipal.name != null}">
								<form id="logoutForm" method="POST"
									action="${contextPath}/logout">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>
								<button type="submit" class="btn btn-primary navbar-btn"
									onclick="document.forms['logoutForm'].submit()">Logout</button>
							</c:if></li>
					</ul>

				</div>
			</div>
		</nav>
		<h3 style="float: right; margin-top: -100px;">
			Welcome <b>${pageContext.request.userPrincipal.name}</b>
		</h3>

		<c:if test="${empty property}">No properties found </c:if>
		<br> <br>
		<div class="container-fluid">
			<c:if test="${not empty property}">
				<center>
					<table class="table table-hover">
						<tr>
							<!-- <th>Id</th> -->
							<th>Property Name</th>
							<th>Flat Type</th>
							<th>Property Address</th>
							<th>Property City</th>
							<th>Property Area</th>
							<th>Landlord Name</th>
							<th>Rent Per Month</th>
							<th>Facilities</th>
							<th>Image</th>
							<th colspan="3" align="center">Action</th>
						</tr>
						<c:forEach items="${property}" var="p">
							<tr>
								<%-- <td>${p.propertyId}</td> --%>
								<td>${p.propertyName}</td>
								<td>${p.flatType}</td>
								<td>${p.propertyAddress}</td>
								<td>${p.propertyCity}</td>
								<td>${p.propertyArea}</td>
								<td>${p.landlordName}</td>
								<td>${p.rentPerMonth}</td>
								<td><c:forEach var="i" items="${p.facilities}">
										<ul>${i}</ul>
									</c:forEach></td>
								<td><img src="${contextPath}/images/${p.image}"
									style="height: 100px; width: 150px; image-rendering: pixelated"></td>
								<td>
									<form
										action="${contextPath}/${p.propertyId}/id/${pageContext.request.userPrincipal.name}"
										method="get">
										<button type="submit" class="btn btn-primary">Edit</button>
									</form>
								</td>
								<td>
									<form
										action="${contextPath}/${p.propertyId}/id/delete/${pageContext.request.userPrincipal.name}"
										method="get">
										<button type="submit" class="btn btn-primary">Delete</button>
									</form>
								</td>
								<td>
									<form action="${contextPath}/${p.propertyId}/id/showrequests"
										method="get">
										<button type="submit" class="btn btn-primary">Show
											Requests</button>
									</form>
								</td>
							</tr>
						</c:forEach>
					</table>
				</center>
			</c:if>
		</div>
		<br> <br>
		<div align="center">
			<form action="${contextPath}/add" method="get">
				<button class="btn btn-primary" type="submit">Add Property</button>
			</form>
		</div>

	</div>
</body>
</html>