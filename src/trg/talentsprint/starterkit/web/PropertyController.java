package trg.talentsprint.starterkit.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import trg.talentsprint.starterkit.model.Property;
import trg.talentsprint.starterkit.model.Requests;
import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.service.PaymentService;
import trg.talentsprint.starterkit.service.PropertyService;
import trg.talentsprint.starterkit.service.RequestService;
import trg.talentsprint.starterkit.service.UserService;

@Controller
public class PropertyController {

	@Autowired
	private PropertyService propertyService;

	@Autowired
	private RequestService requestService;
	
	@Autowired
	private PaymentService paymentService;

	@Value("${upload.location}")
	private String uploadDirectory;

	@GetMapping("/add")
	public String addProp(Model model) {
		model.addAttribute("property", new Property());
		return "new-form";
	}

	@PostMapping(value = "/{name}/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String addNewProperty(@Valid @ModelAttribute("property") Property property, BindingResult result,
			Model model, @RequestParam("file") MultipartFile[] files, @PathVariable("name") String name) {
		if (result.hasErrors()) {
			return "new-form";
		}
		StringBuilder fileNames = new StringBuilder();
		for (MultipartFile file : files) {
			Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
			fileNames.append(file.getOriginalFilename() + " ");
			try {
				Files.write(fileNameAndPath, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String path = fileNames.toString();
		property.setImage(path);
		propertyService.addProperty(property);
		model.addAttribute("property", propertyService.myProperties(name));
		return "myproperties";
	}

	@GetMapping("/view/{name}/name")
	public String viewProperty(Model model, @PathVariable("name") String name) {
		model.addAttribute("property", propertyService.myProperties(name));
		return "myproperties";
	}

	@GetMapping("/landlord-home")
	public String landlordHome(Model model) {
		return "landlord-home";
	}

	@GetMapping("/{propertyId}/id/{name}")
	public String getPropById(Model model, @PathVariable int propertyId) {
		System.out.println();
		Property property = propertyService.getPropertyById(propertyId)
				.orElseThrow(() -> new IllegalArgumentException("Invalid Property Id:" + propertyId));
		model.addAttribute("property", property);
		return "update";
	}

	@PostMapping("/{propertyId}/id/{name}/edit")
	public String editProp(Model model, @PathVariable int propertyId,
			@Valid @ModelAttribute("property") Property property, BindingResult result,
			@RequestParam("file") MultipartFile[] files, @PathVariable("name") String name) {
		if (result.hasErrors())
			System.out.println("Error");
		StringBuilder fileNames = new StringBuilder();
		for (MultipartFile file : files) {
			Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
			fileNames.append(file.getOriginalFilename() + " ");
			try {
				Files.write(fileNameAndPath, file.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String path = fileNames.toString();
		property.setImage(path);
		propertyService.addProperty(property);
		model.addAttribute("property", propertyService.myProperties(name));
		return "myproperties";
	}

	@GetMapping("/{propertyId}/id/delete/{name}")
	public String deleteProperty(Model model, @PathVariable int propertyId, @PathVariable("name") String name) {
		propertyService.getPropertyById(propertyId)
				.orElseThrow(() -> new IllegalArgumentException("Invalid Property Id:" + propertyId));
		propertyService.deletePropertyById(propertyId);
		model.addAttribute("property", propertyService.myProperties(name));
		return "myproperties";
	}


	@GetMapping("/OtherProperties/{username}/name")
	public String getOtherUsersProperties(Model model, @PathVariable("username") String user) {
		model.addAttribute("property", propertyService.getOtherUserProperties(user));
		model.addAttribute("pCity", propertyService.getPropertyCities());
		model.addAttribute("fType", propertyService.getFlatTypes());
		return "tenant-home";
	}

	@GetMapping("/{username}/propByCityAndType/search")
	public String getPropertyByCityOrType(Model model, @RequestParam("flatType") String flatType,
			@RequestParam("propertyCity") String propertyCity, @PathVariable("username") String username) {
		if (flatType.equals("No preference") && propertyCity.equals("")) {
			model.addAttribute("property", propertyService.getOtherUserProperties(username));
			model.addAttribute("pCity", propertyService.getPropertyCities());
			model.addAttribute("fType", propertyService.getFlatTypes());
			return "tenant-home";
		}
		List<Property> properties = propertyService.getPropertyByCityOrType(flatType, propertyCity, username);
		model.addAttribute("property", properties);
		model.addAttribute("pCity", propertyService.getPropertyCities());
		model.addAttribute("fType", propertyService.getFlatTypes());
		model.addAttribute("propertyCity", propertyCity);
		return "tenant-home";
	}

	@GetMapping("/request/{propertyId}/{username}")
	public String getRequest(Model model, @PathVariable("propertyId") int id, @PathVariable("username") String user) {
		List<Requests> requestList = new ArrayList<Requests>();
		requestList = requestService.getRequests(id);
		for (Requests s : requestList) {
			if (s.getUserName().equalsIgnoreCase(user)) {
				return "requested";
			}
		}

		Property property = propertyService.getPropertyById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid Property Id:" + id));
		model.addAttribute("property", property);
		return "agreement";
	}

	@GetMapping("/accept/{propertyId}/{username}")
	public String acceptAgreement(Model model, @PathVariable("propertyId") int id,
			@PathVariable("username") String user) {
		Requests request = new Requests(id, user);
		requestService.addRequest(request);
		model.addAttribute("property", propertyService.getOtherUserProperties(user));
		model.addAttribute("pCity", propertyService.getPropertyCities());
		model.addAttribute("fType", propertyService.getFlatTypes());
		return "tenant-home";

	}

	@GetMapping("/decline/{propertyId}/{username}")
	public String declineAgreement(Model model, @PathVariable("propertyId") int id,
			@PathVariable("username") String user) {
		model.addAttribute("property", propertyService.getOtherUserProperties(user));
		model.addAttribute("pCity", propertyService.getPropertyCities());
		model.addAttribute("fType", propertyService.getFlatTypes());
		return "tenant-home";
	}

	@GetMapping("/{propertyId}/id/showrequests")
	public String showRequests(Model model, @PathVariable("propertyId") int id) {
		List<Requests> requestList = new ArrayList<Requests>();
		requestList = requestService.getRequests(id);
		model.addAttribute("requests", requestList);
		return "showRequests";

	}
	
	@GetMapping("/makePayment/{name}")
	public String Payment(Model model) {
		return "payment";
	}
	
	@GetMapping("/addpayment/{user}")
	public String addNewPayment(@RequestParam("amount") int amount, @RequestParam("type") String type,
			@RequestParam("date") String date, Model model, @PathVariable("user") String user,
			@RequestParam("name") String name) {
		paymentService.paymentsave(amount, type, user, date, name);
		model.addAttribute("plist", paymentService.myPayments(user));
		return "payment-history";
	}
	
	@GetMapping("/paymenthistory/{user}")
	public String showPaymentHis(@PathVariable("user") String user, Model model) {
		model.addAttribute("plist", paymentService.myPayments(user));
		return "payment-history";
	}

	
}
