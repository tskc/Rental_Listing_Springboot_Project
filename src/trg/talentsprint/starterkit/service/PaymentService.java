package trg.talentsprint.starterkit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Payments;
import trg.talentsprint.starterkit.repository.PaymentsRepository;

@Service
public class PaymentService {
	@Autowired
	private PaymentsRepository paymentRepo;

	public void paymentsave(int amount, String type, String user, String date, String name) {
		// TODO Auto-generated method stub
		paymentRepo.paymentsave(amount, type, user, date, name);
	}

	public List<Payments> myPayments(String user) {
		return paymentRepo.myPayments(user);
	}
}
