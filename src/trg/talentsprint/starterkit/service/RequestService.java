package trg.talentsprint.starterkit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Property;
import trg.talentsprint.starterkit.model.Requests;
import trg.talentsprint.starterkit.repository.RequestRepository;

@Service
public class RequestService {
	@Autowired
	private RequestRepository requestRepo;
	  
	public Requests addRequest(Requests r) {
		return requestRepo.save(r);
	}

	public List<Requests> getRequests(int id) {
		return requestRepo.getRequests(id);
	}

}
