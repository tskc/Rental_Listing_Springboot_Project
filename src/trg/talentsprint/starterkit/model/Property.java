package trg.talentsprint.starterkit.model;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Property {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int propertyId;

	@NotBlank
	private String propertyName;

	@NotBlank
	private String flatType;
	@NotBlank
	private String propertyAddress;
	private String propertyCity;
	private int propertyArea;
	@NotBlank
	private String landlordName;
	private long rentPerMonth;
	private String[] facilities;
	private String image;

	public Property() {
		super();
	}

	public Property(int propertyId, @NotBlank String propertyName, @NotBlank String flatType,
			@NotBlank String propertyAddress, String propertyCity, int propertyArea, @NotBlank String landlordName,
			long rentPerMonth, String[] facilities, String image) {
		super();
		this.propertyId = propertyId;
		this.propertyName = propertyName;
		this.flatType = flatType;
		this.propertyAddress = propertyAddress;
		this.propertyCity = propertyCity;
		this.propertyArea = propertyArea;
		this.landlordName = landlordName;
		this.rentPerMonth = rentPerMonth;
		this.facilities = facilities;
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public String getFlatType() {
		return flatType;
	}

	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}

	public String getLandlordName() {
		return landlordName;
	}

	public void setLandlordName(String landlordName) {
		this.landlordName = landlordName;
	}

	public long getRentPerMonth() {
		return rentPerMonth;
	}

	public void setRentPerMonth(long rentPerMonth) {
		this.rentPerMonth = rentPerMonth;
	}

	public String getPropertyCity() {
		return propertyCity;
	}

	public void setPropertyCity(String propertyCity) {
		this.propertyCity = propertyCity;
	}

	public int getPropertyArea() {
		return propertyArea;
	}

	public void setPropertyArea(int propertyArea) {
		this.propertyArea = propertyArea;
	}

	public String[] getFacilities() {
		return facilities;
	}

	public void setFacilities(String[] facilities) {
		this.facilities = facilities;
	}

	@Override
	public String toString() {
		return "Property [propertyId=" + propertyId + ", propertyName=" + propertyName + ", flatType=" + flatType
				+ ", propertyAddress=" + propertyAddress + ", propertyCity=" + propertyCity + ", propertyArea="
				+ propertyArea + ", landlordName=" + landlordName + ", rentPerMonth=" + rentPerMonth + ", facilities="
				+ Arrays.toString(facilities) + "]";
	}

}
