package trg.talentsprint.starterkit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import trg.talentsprint.starterkit.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    
    @Modifying
	@Transactional
	@Query(value = "update user set email_id=?1,city=?2,image=?3 where username=?4", nativeQuery = true)
	void updateUser(String emailId, String city, String image,String username);

}
